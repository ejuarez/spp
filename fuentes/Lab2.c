/**
 * Lab 2: Variables and operators
 * @author ____________________
 * Student Id: _____________________
 * @version  1.0
 * @date ___________
 **/

//Include libraries
#include <stdlib.h> //Standard library
#include <stdio.h> //Standard I/O (input / output) library

/**
 * Main function of the program
 * The execution of the algorithm starts here
 */
int main() {

    //1. Declaration of an integer variable named "variable"
    int variable = 0;

    //2. Ask the user for a value
    printf("Write a value -> ");
    //3. Read the value from the terminal and save it in variable
    scanf("%i%*c", &variable);
    //4. Print the value of variable
    printf("\nThe value is: %i\n\n", variable);
}
