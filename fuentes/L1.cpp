/**
 * @author ________________________
 * Matr�cula: _____________________
 * T�tulo : Mi primer programa en C
 * Objetivo : Familiarizarse con el IDE Dev-C++
 */

#include <stdio.h> // Llamada a librer�a de I/O (Input/Output)
#include <stdlib.h> // Llamada a la librer�a estandard

/*
 * Funci�n principal del programa
 * Aqu� inicia la ejecuci�n del algoritmo
 */
int main()
{
  /*
   * Instrucciones para imprimir mensajes en la pantalla
   */
  printf("\n SOLUCION DE PROBLEMAS CON PROGRAMACION \n\n");
  printf("\t Mi primer programa en C \n\n\n");

  system("PAUSE"); // Instrucci�n que frena o pausa la ejecuci�n del programa
}



