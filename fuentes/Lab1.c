/**
 * Lab 1: My first program in the C programming language
 *   This is a hello world program
 * @author ____________________
 * Student Id: _____________________
 * @version  1.0
 * @date ___________
 **/

//Include libraries
#include <stdlib.h> //Standard library
#include <stdio.h> //Standard I/O (input / output) library

/**
 * Main function of the program
 * The execution of the algorithm starts here
 */
int main() {

    /**
     * Statements to print messages in the terminal
     */
    printf("\n PROBLEM SOLVING WITH PROGRAMMING \n\n\n");
    printf("\t Hello World! \n\n\n\n");
    printf("This is my first program in C \n\n\n");

    system("pause"); //System call to pause the execution of the program
}
